# Demo
[link](https://sql-visualizer-123.herokuapp.com/)

# Requirements

- Take a SQL query as input from user
- Transform a given SQL query into its table-relationship execution flow
- Visualize the execution flow into a diagram

# Assumptions
- The input SQL query is in PostgreSQL dialect.
- The input SQL query is always valid.
- With limited knowledge of SQL, we are going to focus on the 2 examples provided in the assignment first: SELECT statements only.

# Design

## Data flow
  - Raw SQL query
  - SQL Query tree
  - Generic execution flow
  - Specific graph for visualization

## Components
  - Client:
    - Take SQL query from user as input
    - Visualize the execution flow and present it to user
  - Server:
    - Transform a SQL query into a raw SQL query tree
    - Transform a raw SQL query tree into a refined SQL query tree
    - Transform a SQL query tree into a generic execution tree
    - Transform a generic execution tree into a specific specific graph
  - The client and server communicate via REST

```
  Client                             Server
    |                                   |
    |---------- Raw SQL Query --------->|
    |                                   |
    |                                   |--------
    |                                   |       | raw SQL Query tree
    |                                   |<-------
    |                                   |
    |                                   |--------
    |                                   |       | refined SQL query tree
    |                                   |<-------
    |                                   |
    |                                   |--------
    |                                   |       | Generic execution tree
    |                                   |<-------
    |                                   |
    |<---------- Specific graph ------- |
    |                                   |
```

# Technology choices

## Server
- `pg-query-parser` is selected to parse SQL queries because it ultilizes the query parser from PostgreSQL.
  - Regular expression came to mind at first. However, for such a complex language as SQL, regular expression is not sufficient.
  - Other libraries such as `pg_query` were also considered. However, they require more unfamiliar tools.
- `express` was selected for the web server because of its simplicity.

## Client
- `Angular` was selected for the client application because of my proficiency in it.
- `mermaid` was selected to visualize the graph because of its simplicity.
  - GoJS, no go as it is not free
  - mxgraph is too complicated, not fit in our time constraint.
  - cytoscape looks simpler than mxgraph, but still quite complicated.

# Implementation

## sql-visualizer-transformer [link](https://gitlab.com/tnt2411/sql-visualizer-transformer)
This module contains the following transformers:
- `Refiner`: SQL query -> SQL trees
- `Transformer`: SQL tree -> execution flow tree
- `MermaidTransformer`: SQL query -> mermaid graphs

## sql-visualizer [link](https://gitlab.com/tnt2411/sql-visualizer)
This module contains the web server and the client application

### Server
- Ultilizing transformers to provide the client with graphs in specific formats, such as mermaid.
- REST API

### Client
- `sv-form`: the form component responsible for taking SQL query input from user
- `sv-mermaid`: the visualization component ultilizing `mermaid`

# Progress
- `Transformer` is not completed.
- Other components are linked and mocking is used to fill in for `Transformer`.
- Needed time to finish: approximately 5 hours.

# Reflection
- The starting scope was too large. More specifically, the decision to implement `Refiner` and `Transformer` as complex parsers costed too many resources.
  - Alternative: support specific statement formats and use regular expression to parse.

# Further work
- Add more unit tests.
- Expand the scope to more type of statements.

# Challenges
- Dependency resolution while transforming refined SQL trees to execution flow trees.
  - Solution: perform a topological sorting
  - Alternative: make an assumption that the CTEs in every SQL trees are in topological order.
