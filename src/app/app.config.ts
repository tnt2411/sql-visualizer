import { InjectionToken } from '@angular/core';
import { IConfig } from '../environments/config';

export { IConfig } from '../environments/config';

export const CONFIG = new InjectionToken<IConfig>('config');
