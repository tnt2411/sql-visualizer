import { Component, OnInit, ElementRef, Input, OnChanges } from '@angular/core';
import { MermaidService } from './mermaid.service';

@Component({
  selector: 'sv-mermaid',
  templateUrl: './mermaid.component.html',
  styleUrls: ['./mermaid.component.sass']
})
export class MermaidComponent implements OnInit, OnChanges {

  @Input() public definition: string;

  public graph: string;

  constructor(private mermaid: MermaidService, private element: ElementRef) { }

  ngOnInit() {
    this.update();
  }

  ngOnChanges() {
    this.update();
  }

  update() {
    this.graph = this.mermaid.render(this.definition, this.element);
  }

}
