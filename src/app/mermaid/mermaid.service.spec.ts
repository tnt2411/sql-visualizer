import { TestBed, inject } from '@angular/core/testing';

import { MermaidService } from './mermaid.service';

describe('MermaidService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MermaidService]
    });
  });

  it('should be created', inject([MermaidService], (service: MermaidService) => {
    expect(service).toBeTruthy();
  }));
});
