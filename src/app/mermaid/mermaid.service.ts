import * as mermaid from 'mermaid';
import { Injectable, Sanitizer, SecurityContext, ElementRef } from '@angular/core';

@Injectable()
export class MermaidService {

  constructor(private sanitizer: Sanitizer) {
    mermaid.initialize({
      startOnLoad: false
    });
  }

  public render(graph: string, element: ElementRef) {
    return this.sanitizer.sanitize(SecurityContext.HTML, mermaid.render(this.id(), graph, null, element.nativeElement));
  }

  private id() {
    return 'mermaid_' + Math.random().toString(36).substr(2, 9);
  }
}
