import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MermaidService } from './mermaid.service';
import { MermaidComponent } from './mermaid.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MermaidComponent],
  exports: [MermaidComponent],
  providers: [
    MermaidService
  ]
})
export class MermaidModule { }
