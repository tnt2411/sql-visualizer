import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiService } from './api.service';

@Injectable()
export class QueryService {

  constructor(private api: ApiService) { }

  public post(query: string) {
    return this.api.post('/api/query', { query });
  }

}
