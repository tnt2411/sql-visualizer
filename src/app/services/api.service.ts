import { Injectable, Inject } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import { UrlService } from './url.service';

@Injectable()
export class ApiService {
  constructor( @Inject(Http) private http: Http, private url: UrlService) { }

  public get(path: string, options?: RequestOptionsArgs) {
    return this.http.get(this.url.url(path), options);
  }

  public head(path: string, options?: RequestOptionsArgs) {
    return this.http.head(this.url.url(path), options);
  }

  public post(path: string, data: any, options?: RequestOptionsArgs) {
    return this.http.post(this.url.url(path), data, options);
  }

  public put(path: string, data: any, options?: RequestOptionsArgs) {
    return this.http.put(this.url.url(path), data, options);
  }

  public delete(path: string, options?: RequestOptionsArgs) {
    return this.http.delete(this.url.url(path), options);
  }

  public options(path: string, options?: RequestOptionsArgs) {
    return this.http.options(this.url.url(path), options);
  }

  public patch(path: string, data: any, options?: RequestOptionsArgs) {
    return this.http.patch(this.url.url(path), data, options);
  }
}
