import { NgModule } from '@angular/core';
import { ApiService } from './api.service';
import { QueryService } from './query.service';
import { UrlService } from './url.service';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    HttpModule
  ],
  declarations: [],
  providers: [
    UrlService,
    ApiService,
    QueryService
  ]
})
export class ServicesModule { }
