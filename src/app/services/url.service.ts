import { Injectable, Inject } from '@angular/core';
import { CONFIG, IConfig } from '../app.config';

@Injectable()
export class UrlService {

  constructor( @Inject(CONFIG) private config: IConfig) { }

  public url(path: string) {
    return `${this.config.endpoint}${path}`;
  }
}
