import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ServicesModule } from './services/services.module';

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';

import { environment } from '../environments/environment';
import { CONFIG } from './app.config';
import { MermaidModule } from './mermaid/mermaid.module';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ServicesModule,
    MermaidModule
  ],
  providers: [
    { provide: CONFIG, useValue: environment },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
