import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'sv-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  @Output() public postSubmit: EventEmitter<string> = new EventEmitter<string>();

  public query: string;
  @ViewChild('form') public form: NgForm;

  public ngOnInit() {
    this.query = `with daily_users as (
  select
    created_at::date as date_d,
    count(1) as new_users
  from users
  group by 1
), daily_products as (
  select
    created_at::date as date_d,
    count(1) as new_users
  from products
  group by 1

), daily_reviews as (
  select
    created_at::date as date_d,
    count(1) as new_users
  from reviews
  group by 1
)

select
  U.date_d,
  U.new_users,
  P.new_products,
  R.new_reviews
from daily_users U, daily_products P, daily_reviews R
where U.date_d = P.date_d
  and U.date_d = R.date_d
order by 1 DESC;

WITH user_orders AS (
  select
  user_id,
  count(1) as order_count
  from orders O
  where O.created_at > NOW() - INTERVAL '30 days'
  group by 1
)
select
  order_count,
  count(1) as num_users
from user_orders
group by 1
order by 1;
`;
  }

  public onSubmit() {
    if (this.form.form.valid) {
      this.postSubmit.emit(this.query);
    }
  }
}
