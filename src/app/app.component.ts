import { Component } from '@angular/core';
import { QueryService } from './services/query.service';

@Component({
  selector: 'sv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  constructor(private query: QueryService) { }

  public graphs: string[];
  public error: string;

  public onSubmit(query: string) {
    this.query.post(query)
      .subscribe((r) => {
        this.graphs = r.json().data;
        this.error = undefined;
      }, (error) => {
        this.graphs = [];
        this.error = error.json().error;
      });
  }
}
