export interface IConfig {
  endpoint: string;
  production: boolean;
}
