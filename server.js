const express = require('express');
const parser = require('pg-query-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const transformers = require('sql-visualizer-transformer');
const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(express.static(__dirname + '/dist'));

const server = app.listen(process.env.PORT || 8080, function () {
  const port = server.address().port;
  console.log('App now running on port', port);
});

app.options('*', cors()) // include before other routes

/**
 * /api/query
 *   POST: creates a new execution tree
 */
app.post('/api/query', function (req, res) {
  const query = req.body && req.body.query;

  if (!query) {
    hanldeUserInputError(res, 'Must provide a PostgreSQL query.');
  } else if (typeof query !== 'string') {
    hanldeUserInputError(res, 'Query must be a string');
  }

  const transformer = new transformers.MermaidTransformer();
  transformer.transform(query)
    .fold(() => handleError(res, 'Unable to parsed the given query', parsed.error.message, 400),
      (graphs) => res.status(200).json({
        data: graphs
      }));
});

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log('ERROR: ' + reason);
  res.status(code || 500).json({
    'error': message
  });
}

// Error handler for user input error
function hanldeUserInputError(response, message) {
  handleError(res, 'Invalid user input', message, 400);
}
